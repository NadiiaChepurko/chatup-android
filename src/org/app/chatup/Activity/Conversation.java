package org.app.chatup.Activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.activeandroid.query.Select;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.ion.Ion;
import org.apache.commons.lang3.StringUtils;
import org.app.chatup.Adapter.MessagesListAdapter;
import org.app.chatup.AsyncTask.AbstractGetGoogleAccessTokenTask;
import org.app.chatup.DB.Messages;
import org.app.chatup.DB.Websites;
import org.app.chatup.R;
import org.app.chatup.Util.Constants;
import org.app.chatup.Util.Util;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Date;

/**
 * Created by Vicky on 1/8/15.
 */
public class Conversation extends ActionBarActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private ImageButton sendButton;
    private EditText messageInput;
    private ListView listViewMessages;
    private MessagesListAdapter adapter;

    protected GoogleApiClient mGoogleApiClient;
    protected android.location.Location mLastLocation;

    private String gcm_regId;
    Cursor cursor;

    private static final String TAG = "ConversationActivity";
    private android.support.v7.widget.Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversation);

        if(Constants.appContext == null) {
            Constants.appContext = getApplicationContext();
        }
        if(Constants.keystorePass == null) {
            Constants.keystorePass = getResources().getString(R.string.keystorePass_ec2);
//            Constants.keystorePass = getResources().getString(R.string.keystorePass_win);
        }

        messageInput = (EditText) findViewById(R.id.messageEdit);
        listViewMessages = (ListView) findViewById(R.id.listView_messages);
        listViewMessages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView lblFrom = (TextView) view.findViewById(R.id.lblMsgFrom);
                TextView lblMessage = (TextView) view.findViewById(R.id.txtMsg);
                String message = lblMessage.getText().toString();

                if(lblFrom.getText().toString().equals("You")) {
                    messageInput.setText(message);
                }
            }
        });
        adapter = new MessagesListAdapter(this, cursor, 0);
        listViewMessages.setAdapter(adapter);

        sendButton = (ImageButton) findViewById(R.id.btn_send);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String message = messageInput.getText().toString().trim();
                if((! isDeviceRegistered()) || (message.length() < 1)) {
                    return ;
                }
                // Add to listView and save to Database
                saveMessageToDb(message, true);

                if("Cloud Storage".equals(Constants.currentVertical)) {
                    new RetrieveGoogleTokenTask(Conversation.this,
                            Util.getProperty(Constants.PROPERTY_EMAIL, Conversation.this, TAG),
                            Constants.DRIVE_SCOPE).execute();

                } else if("Google Calendar".equals(Constants.currentVertical)) {
                    new RetrieveGoogleTokenTask(Conversation.this,
                            Util.getProperty(Constants.PROPERTY_EMAIL, Conversation.this, TAG),
                            Constants.CALENDAR_SCOPE + Constants.CONTACTS_SCOPE.replace("oauth2:", " ")).execute();

                } else {
                    selfSigned();
                }
            }
        });

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // update title
                toolbar.setTitle(Constants.currentVertical);
                refreshListView();
            }
        };
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadcastReceiver, new IntentFilter("newMessageIntent"));

        // Update the tool bar title with the TypefaceSpan instance
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar_conversation);
        toolbar.setTitle(Constants.currentVertical);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buildGoogleApiClient();
    }

    private Bundle getExtras(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return getIntent().getExtras();
        } else {
            return savedInstanceState;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_conversation, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if("Cloud Storage".equals(Constants.currentVertical)) {
            Log.d("DEBUG", "onPrepareOptionsMenu");
            MenuItem item_google_drive = menu.findItem(R.id.google_drive);
            MenuItem item_one_drive = menu.findItem(R.id.one_drive);

            if ("".equals(Util.getProperty(Constants.PROPERTY_GOOGLE_DRIVE_ACCESS_TOKEN, Conversation.this, TAG).trim())) {
                item_google_drive.setVisible(true);
            } else {
                item_google_drive.setVisible(false);
            }

            if ("".equals(Util.getProperty(Constants.PROPERTY_ONE_DRIVE_ACCESS_TOKEN, Conversation.this, TAG).trim())) {
                item_one_drive.setVisible(true);
            } else {
                item_one_drive.setVisible(false);
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;

            case R.id.google_drive:
                // Go get the auth token
                new RetrieveGoogleTokenTask(Conversation.this,
                        Util.getProperty(Constants.PROPERTY_EMAIL, Conversation.this, TAG),
                        Constants.DRIVE_SCOPE).execute();
                return true;

            case R.id.one_drive:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    @Override
    protected void onResume() {
        super.onResume();

        // Clear notification if present
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Constants.NOTIFICATION_ID);
        refreshListView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Messages.markAllAsRead(Constants.currentVertical);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQ_RECOVER_PLAY_SERVICES_ERROR) {

            if(resultCode == RESULT_OK) {
                // We had to sign in - now we can finish off the token request.
                new RetrieveGoogleTokenTask(this,
                        Util.getProperty(Constants.PROPERTY_EMAIL, Conversation.this, TAG),
                        Constants.DRIVE_SCOPE).execute();
            } else {
                Toast.makeText(Conversation.this, getResources().getString(R.string.auth_cancel), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class RetrieveGoogleTokenTask extends AbstractGetGoogleAccessTokenTask {
        private static final String TAG = "RetrieveGoogleTokenTask";

        public RetrieveGoogleTokenTask(Activity activity, String email, String scope) {
            super(activity, email, scope);
        }

        @Override
        protected void handleResult(String token, final String scope) {
            if(token != null) {
                if(scope.contains(Constants.DRIVE_SCOPE)) {
                    Util.saveToPrefs(token, Constants.PROPERTY_GOOGLE_DRIVE_ACCESS_TOKEN, mActivity);
                } else if(scope.contains(Constants.CALENDAR_SCOPE)) {
                    Util.saveToPrefs(token, Constants.PROPERTY_GOOGLE_CALENDAR_ACCESS_TOKEN, mActivity);
                }

                Log.i(TAG, "Access Token saved");
                Log.d("DEBUG", token);
                // Hide Google Drive icon in Action Bar since the user has granted access
                invalidateOptionsMenu();

                selfSigned();
            }
        }
    }

    /**
     * Checks GCM registration
     * @return  true if device is successfully registered for GCM. False otherwise.
     */
    private boolean isDeviceRegistered() {
        gcm_regId = Util.getProperty(Constants.PROPERTY_GCM_REG_ID, Conversation.this, TAG);
        if("".equals(gcm_regId)) {
            Toast.makeText(Conversation.this, "Device is not registered", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * This mess will go away soon. It had to done since ChatUp doesn't have SSL certificate.
     * Currently using self-signed ones.
     */
    private void selfSigned() {

        KeyManagerFactory kmf = null;
        KeyStore ks = null, ts = null;
        TrustManagerFactory tmf = null;
        SSLContext sslContext = null;
        Ion ion = Ion.getDefault(Conversation.this);

        try {
            kmf = KeyManagerFactory.getInstance("X509");
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(getResources().openRawResource(R.raw.chatupcert_ec2), Constants.keystorePass.toCharArray());
//            ks.load(getResources().openRawResource(R.raw.chatupcert_win), Constants.keystorePass.toCharArray());
            kmf.init(ks, Constants.keystorePass.toCharArray());

            tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            ts = KeyStore.getInstance(KeyStore.getDefaultType());
            ts.load(getResources().openRawResource(R.raw.chatupcert_ec2), Constants.keystorePass.toCharArray());
//            ts.load(getResources().openRawResource(R.raw.chatupcert_win), Constants.keystorePass.toCharArray());
            tmf.init(ts);

            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            ion.configure().createSSLContext("TLS");
            ion.getHttpClient().getSSLSocketMiddleware().setTrustManagers(tmf.getTrustManagers());
            ion.getHttpClient().getSSLSocketMiddleware().setSSLContext(sslContext);

            JsonObject jsonMessageToServer = buildMessageJson(messageInput.getText().toString());
            Log.d(TAG, jsonMessageToServer.toString());

            ion.build(Conversation.this)
                    .load(Constants.SERVER_HANDLER_URL)
                    .setJsonObjectBody(jsonMessageToServer)
                    .asJsonObject()
            ;
            messageInput.getText().clear();
            Log.d("DEBUG", jsonMessageToServer.toString());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Appending message to list view
     */
     private void refreshListView() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                cursor = Messages.fetchConversationsFromTitle(Constants.currentVertical);
                adapter.swapCursor(cursor);
            }
        });
    }

    /**
     * Builds a JSON object to send to the server.
     * @param message   EditText value - query to be sent to server
     * @return          JSON object with query and metadata
     * @throws JSONException
     */
    private JsonObject buildMessageJson(String message) throws JSONException {
        JSONObject innerJson = new JSONObject();
        JSONObject outerJson = new JSONObject();
        JSONObject locationJson = new JSONObject();

        if (mLastLocation == null) {
            Log.e(TAG, "mLastLocation is NULL!!!");
        }
        locationJson.put("lat", mLastLocation.getLatitude());
        locationJson.put("lng", mLastLocation.getLongitude());
        innerJson.put("urlTitle", Constants.currentVertical);
        innerJson.put("message", message.trim());

        outerJson.put("data", innerJson);
        outerJson.put("gcm_regId", gcm_regId);
        outerJson.put("location", locationJson);

        String DRIVE_TOKEN = Util.getProperty(Constants.PROPERTY_GOOGLE_DRIVE_ACCESS_TOKEN, this, TAG);
        String CALENDAR_TOKEN = Util.getProperty(Constants.PROPERTY_GOOGLE_CALENDAR_ACCESS_TOKEN, this, TAG);

        if(! "".equals(DRIVE_TOKEN)) {
            outerJson.put("google_drive_access_token", DRIVE_TOKEN);
        }
        if(! "".equals(CALENDAR_TOKEN)) {
            outerJson.put("google_calendar_access_token", CALENDAR_TOKEN);
        }

        return (JsonObject) new JsonParser().parse(String.valueOf(outerJson));
    }

    public void saveMessageToDb(String message, boolean is_from_user) {
        // Should never happen
        if(Constants.currentVertical == null) {
            Log.wtf(TAG, "saveMessageToDb: currentVertical is NULL");
            return;
        }

        Websites website = new Select("*, Id as _id")
                .from(Websites.class)
                .where("website_title = ?", Constants.currentVertical)
                .executeSingle();

        // Update last conversation time to sort list by time
        website.last_conversation_time = new Date();
        website.save();

        new Messages(message, website.getId(), is_from_user).save();
        refreshListView();
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        Bundle extras = getExtras(null);
        String message = extras.getString(Constants.EXTRA_MESSAGE);
        if (StringUtils.isNotBlank(message)) {
            messageInput.setText(message);
            sendButton.performClick();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }
}