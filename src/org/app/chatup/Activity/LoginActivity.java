package org.app.chatup.Activity;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import org.app.chatup.AsyncTask.AbstractGetGoogleAccessTokenTask;
import org.app.chatup.AsyncTask.GCMRegisterTask;
import org.app.chatup.R;
import org.app.chatup.Util.AsyncResponse;
import org.app.chatup.Util.Constants;
import org.app.chatup.Util.Util;

/**
 * Created by Vicky on 3/18/15.
 */
public class LoginActivity extends Activity implements AsyncResponse {

    protected static final String TAG = "LoginActivity";
    protected String mAccountName;
    protected Button signInBtn;
    private GCMRegisterTask gcmRegisterTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Constants.appContext == null) {
            Constants.appContext = getApplicationContext();
        }
        if(Constants.keystorePass == null) {
            Constants.keystorePass = getResources().getString(R.string.keystorePass_ec2);
//            Constants.keystorePass = getResources().getString(R.string.keystorePass_win);
        }

        checkPlayServices();

        if(! "".equals(Util.getProperty(Constants.PROPERTY_GOOGLE_ACCESS_TOKEN, this, TAG))) {
            checkGCMRegistration();
        }

        setContentView(R.layout.startup_login);
        signInBtn = (Button) findViewById(R.id.sign_in_button);
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickUserAccount();
            }
        });
    }

    private void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        startActivityForResult(intent, Constants.REQUEST_CODE_PICK_ACCOUNT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Constants.REQUEST_CODE_PICK_ACCOUNT) {
            if(resultCode == RESULT_OK) {
                mAccountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                Util.saveToPrefs(mAccountName, Constants.PROPERTY_EMAIL, this);
                // With the account name (email) acquired, go get the auth token
                new RetrieveGoogleTokenTask(this, mAccountName, Constants.LOGIN_SCOPE).execute();

            } else if(resultCode == RESULT_CANCELED) {
                // The account picker dialog closed without selecting an account.
                // Notify users that they must pick an account to proceed.
                Toast.makeText(this, R.string.pick_account_cancel, Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == Constants.REQ_RECOVER_PLAY_SERVICES_ERROR && resultCode == RESULT_OK) {
            // We had to sign in - now we can finish off the token request.
            new RetrieveGoogleTokenTask(this, mAccountName, Constants.LOGIN_SCOPE).execute();
        }
    }

    private class RetrieveGoogleTokenTask extends AbstractGetGoogleAccessTokenTask {
        private static final String TAG = "RetrieveGoogleTokenTask";

        public RetrieveGoogleTokenTask(Activity activity, String email, String scope) {
            super(activity, email, scope);
        }

        @Override
        protected void handleResult(String token, String scope) {
            if(token != null) {
                Util.saveToPrefs(token, Constants.PROPERTY_GOOGLE_ACCESS_TOKEN, mActivity);
                Log.i(TAG, "Access Token saved");
                checkGCMRegistration();
            } else {
                show(getResources().getString(R.string.auth_error));
            }
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /** Checks whether the device currently has a network connection */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    public static SharedPreferences getGcmPreferences(Context context) {
        return context.getSharedPreferences(context.getResources().getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    public static String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(Constants.PROPERTY_GCM_REG_ID, "");
        if ("".equals(registrationId.trim())) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        return registrationId;
    }


    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }

    /**
     * Checks if this device is registered for GCM service
     */
    private void checkGCMRegistration() {
        if("".equals(getRegistrationId(this).trim())) {
            gcmRegisterTask = new GCMRegisterTask(this);
            gcmRegisterTask.delegate = this;
            gcmRegisterTask.execute();
        } else {
            goToMainActivity();
        }
    }

    public void asyncTaskFinish(String asyncOutput) {
        if("OK".equals(asyncOutput)) {
            goToMainActivity();
        } else {
            Toast.makeText(this, "Error registering your device. Please try again later", Toast.LENGTH_SHORT).show();
            this.finish();
        }
    }
}