package org.app.chatup.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import org.app.chatup.Client.DropboxClient;
import org.app.chatup.Client.FoursquareClient;
import org.app.chatup.Client.MicrosoftClient;
import org.app.chatup.Client.UberClient;
import org.app.chatup.R;

/**
 * This Fragment displays the status of supported accounts (connected or not).
 */
public class ConnectedAccountsActivity extends FragmentActivity {

    FoursquareClient foursquareClient = null;
    MicrosoftClient microsoftClient = null;
    DropboxClient dropboxClient = null;
    UberClient uberClient = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connect_accounts);

        displayFoursquareStatus();
        displayMicrosoftStatus();
        displayDropboxStatus();
        displayUberStatus();
    }

    /**
     * Check if Foursquare account is connected
     */
    private void displayFoursquareStatus() {
        if(foursquareClient == null) {
            foursquareClient = (FoursquareClient) FoursquareClient.getInstance(FoursquareClient.class, this);
        }
    }

    /**
     * Check if Microsoft Live account is connected
     */
    private void displayMicrosoftStatus() {
        if(microsoftClient == null) {
            microsoftClient = (MicrosoftClient) MicrosoftClient.getInstance(MicrosoftClient.class, this);
        }
    }

    /**
     * Check if DropBox account is connected
     */
    private void displayDropboxStatus() {
        if(dropboxClient == null) {
            dropboxClient = (DropboxClient) DropboxClient.getInstance(DropboxClient.class, this);
        }
    }

    /**
     * Check if Uber account is connected
     */
    private void displayUberStatus() {
        if(uberClient == null) {
            uberClient = (UberClient) UberClient.getInstance(UberClient.class, this);
        }
    }
}