package org.app.chatup.Activity;

import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.*;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import org.app.chatup.Adapter.WebsitesListAdapter;
import org.app.chatup.DB.Websites;
import org.app.chatup.R;
import org.app.chatup.Util.Constants;
import org.app.chatup.Util.Util;


public class MainActivity extends ActionBarActivity {

    private ListView listView;
    private TextView emptyListText;
    private WebsitesListAdapter listViewAdapter;
    private android.support.v7.widget.Toolbar toolbar;
    private Cursor websitesCursor;
    private boolean search_toggle;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.websites_list);

        listView = (ListView) findViewById(R.id.list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                websiteOnClick(view);
            }
        });

        emptyListText = (TextView) findViewById(R.id.emptyListText);
        search_toggle = true;
        /**
         * When app is launched for 1st time, add default URLs to SharedPrefs.
         * Else, read from SharedPrefs
         */
        SharedPreferences prefs = getSharedPreferences("first_time", Context.MODE_PRIVATE);
        if (prefs.getBoolean("my_first_time", true)) {
            prefs.edit().putBoolean("my_first_time", false).commit();
            addDefaultUrls();
        }

        // Update the tool bar title with the TypefaceSpan instance
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("ChatUp");
        setSupportActionBar(toolbar);

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals("newMessageIntent")) {
                    websitesCursor = Websites.fetchAddedWebsites();
                    refreshListView();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("newMessageIntent");
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_connected_accounts:
                startActivity(new Intent(getBaseContext(), ConnectedAccountsActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView search = (SearchView) MenuItemCompat.getActionView(searchItem);
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(query != null && query.length() > 1) {
                    websitesCursor = Websites.SearchWebsitesByTitle(query);
                } else {
                    websitesCursor = Websites.fetchAddedWebsites();
                }

                if(websitesCursor.getCount() < 1) {
                    emptyListText.setVisibility(View.VISIBLE);
                } else {
                    emptyListText.setVisibility(View.GONE);
                }

                refreshListView();

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(search_toggle) {
            websitesCursor = Websites.fetchAddedWebsites();
        } else {
            search_toggle = true;
        }

        if(websitesCursor.getCount() > 0) {
            emptyListText.setVisibility(View.GONE);
        }

        listViewAdapter = new WebsitesListAdapter(this, websitesCursor, 0);
        listView.setAdapter(listViewAdapter);

        // Clear notification if present
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Constants.NOTIFICATION_ID);
    }

    /**
     * This needs to be done in Client-Server background sync as we can
     * change default supported websites without updating the client.
     * (Yet to work on this)
     */
    private void addDefaultUrls() {

        // add common chat
        Uri chatImgUrl = Util.getUrl(R.drawable.chat_symbol);
        Websites websites = new Websites("Common", chatImgUrl.toString());
        websites.save();

        websites = new Websites("NJTransit",
                Constants.baseUrl_prefix + "www.njtransit.com" + Constants.baseUrl_suffix
        );
        websites.save();

        websites = new Websites("Winescapes",
                Constants.baseUrl_prefix + "www.winescapes.net" + Constants.baseUrl_suffix
        );
        websites.save();

        websites = new Websites(Constants.WEBSITES_OPEN_TABLE,
                Constants.baseUrl_prefix + "www.opentable.com" + Constants.baseUrl_suffix
        );
        websites.save();

        websites = new Websites(Constants.WEBSITES_YELP,
                Constants.baseUrl_prefix + "www.yelp.com" + Constants.baseUrl_suffix
        );
        websites.save();

        websites = new Websites("Cloud Storage",
                Constants.baseUrl_prefix + "drive.google.com" + Constants.baseUrl_suffix
        );
        websites.save();

        websites = new Websites("NextBus",
                Constants.baseUrl_prefix + "www.nextbus.com" + Constants.baseUrl_suffix
        );
        websites.save();

        websites = new Websites("Google Calendar",
                Constants.baseUrl_prefix + "www.google.com" + Constants.baseUrl_suffix
        );
        websites.save();

        websites = new Websites("Movies",
                Constants.baseUrl_prefix + "www.google.com" + Constants.baseUrl_suffix
        );
        websites.save();

        websites = new Websites("Uber",
                Constants.baseUrl_prefix + "www.uber.com" + Constants.baseUrl_suffix
        );
        websites.save();

/*
        websites = new Websites("Google Tasks",
                Constants.baseUrl_prefix + "www.google.com" + Constants.baseUrl_suffix
        );
        websites.save();
*/
    }


    private void websiteOnClick(View view) {
        TextView textView = (TextView) view.findViewById(R.id.title);
        Constants.currentVertical = textView.getText().toString();

        Util.goToConversationActivity(this, Constants.currentVertical);
    }

    /**
     * Appending message to list view
     */
    private void refreshListView() {
        listViewAdapter.swapCursor(websitesCursor);
    }
}
