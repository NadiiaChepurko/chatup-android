package org.app.chatup.Util;

/**
 * Created by Vicky on 3/21/15.
 */
public interface AsyncResponse {
    /**
     * Used to get the result of AsyncTask.
     * This method will be called once AsyncTask completes execution.
     * @param asyncOutput Result of AsyncTask
     */
    void asyncTaskFinish(String asyncOutput);
}
