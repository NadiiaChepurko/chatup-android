package org.app.chatup.Util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import org.app.chatup.Activity.Conversation;
import org.app.chatup.DB.Websites;
import org.app.chatup.R;

/**
 * Created by Vicky on 3/22/15.
 */
public class Util {

    // convert local resource to url
    public static Uri getUrl(int res){
        return Uri.parse("android.resource://org.app.chatup/" + res);
    }

    /**
     * Gets the requested Property, if there is one.
     *
     * @return Property value, or empty string if there is no existing
     *         token.
     */
    public static String getProperty(String PROPERTY, Context context, String TAG) {
        final SharedPreferences prefs = context.getSharedPreferences(context.getResources().getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);
        String token = prefs.getString(PROPERTY, null);
        if (token == null) {
            Log.i(TAG, "Property " + PROPERTY + " not found.");
            return "";
        }
        return token;
    }

    /**
     * Stores the passed Property in the application's {@code SharedPreferences}.
     *
     * @param token Property value
     */
    public static void saveToPrefs(String token, String PROPERTY, Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(context.getResources().getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY, token);
        editor.commit();
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static void goToConversationActivity(Context context, String urlTitle) {
        goToConversationActivity(context, urlTitle, null);
    }

    public static void goToConversationActivity(Context context, String urlTitle, String message) {
        Constants.currentVertical = urlTitle;
        Cursor cursor = Websites.fetchWebsiteByName(urlTitle);
        int id = cursor.getColumnIndexOrThrow("_id");

        Intent intent = new Intent(context, Conversation.class);
        intent.putExtra("websiteId", id);
        intent.putExtra(Constants.EXTRA_MESSAGE, message);
        context.startActivity(intent);
    }

}
