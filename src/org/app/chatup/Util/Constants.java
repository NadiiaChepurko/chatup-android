package org.app.chatup.Util;

import android.content.Context;

import java.util.List;

/**
 * Created by Vicky on 1/15/15.
 */
public final class Constants {

    /**
     * URL to get Favicon image from domain name
     */
    public static final String localhost = "213.111.122.6";

    public static final String baseUrl_prefix = "http://www.google.com/s2/favicons?domain_url=http%3A%2F%2F";
    public static final String baseUrl_suffix = "%2F";
    public static final String SERVER_REGISTER_URL = "https://52.32.135.72:8443/ChatUp/Register";
    public static final String SERVER_HANDLER_URL = "https://52.32.135.72:8443/ChatUp/RequestHandler";
//    public static final String SERVER_HANDLER_URL = "https://" + localhost + ":8443/ChatUp/RequestHandler";
//    public static final String SERVER_REGISTER_URL = "https://" + localhost + ":8443/ChatUp/Register";

    public static final String EXTRA_MESSAGE = "message";

    /*server client id (create server client in google console)*/
    public static final String LOGIN_SCOPE = "audience:server:client_id:577649254158-7rru5b17s2du2vh7jibs0fogajm26ktr.apps.googleusercontent.com";
    public static final String DRIVE_SCOPE = "oauth2:https://www.googleapis.com/auth/drive";
    public static final String CALENDAR_SCOPE = "oauth2:https://www.googleapis.com/auth/calendar.readonly";
    public static final String CONTACTS_SCOPE = "oauth2:https://www.googleapis.com/auth/contacts.readonly";
    public static String keystorePass;
    public static final String PROPERTY_GCM_REG_ID = "gcm_registration_id";
    public static final String PROPERTY_EMAIL = "email";
    public static final String PROPERTY_APP_VERSION = "app_version";
    public static final String PROPERTY_GOOGLE_ACCESS_TOKEN = "google_access_token";
    public static final String PROPERTY_GOOGLE_DRIVE_ACCESS_TOKEN = "google_drive_access_token";
    public static final String PROPERTY_GOOGLE_CALENDAR_ACCESS_TOKEN = "google_calendar_access_token";
    public static final String PROPERTY_ONE_DRIVE_ACCESS_TOKEN = "one_drive_access_token";
    public static final String PROPERTY_FOURSQUARE_ACCESS_TOKEN = "foursquare_access_token";

    public static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    public static final int REQ_RECOVER_PLAY_SERVICES_ERROR = 1001;
    public static final int REQ_AUTH_REQUIRED = 1002;
    public static final int REQUEST_CODE_RECOVER_FROM_AUTH_ERROR = 2001;
    public static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 2002;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static String WEBSITES_COMMON = "Common";
    public static String WEBSITES_YELP = "Yelp";
    public static String WEBSITES_OPEN_TABLE = "Opentable";

    public static String WEBSITES_OPEN_TABLE_MESSAGE_TEMPLATE = "%s %s";

    public static Context appContext;
    // Holds the title of current vertical (website title)
    public static String currentVertical;

    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    public static final String SENDER_ID = "577649254158";
    public static final int NOTIFICATION_ID = 1;

}
