package org.app.chatup.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.app.chatup.Client.DropboxClient;
import org.app.chatup.R;

/**
 * Created by Vicky on 5/9/15.
 */
public class DropBoxLoginFragment extends BaseServiceLoginFragment<DropboxClient> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dropbox_login, container);

        return view;
    }

    @Override
    public void onCreate(Bundle saved) {
        super.onCreate(saved);
        setServiceName("Dropbox");
        setButtonResId(R.id.btn_dropboxLogin);
    }
}