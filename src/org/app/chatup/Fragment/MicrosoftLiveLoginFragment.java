package org.app.chatup.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.app.chatup.Client.MicrosoftClient;
import org.app.chatup.R;

/**
 * Created by Vicky on 5/9/15.
 */
public class MicrosoftLiveLoginFragment extends BaseServiceLoginFragment<MicrosoftClient> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_microsoft_login, container);

        return view;
    }

    @Override
    public void onCreate(Bundle saved) {
        super.onCreate(saved);
        setServiceName("Microsoft");
        setButtonResId(R.id.btn_microsoftLogin);
    }
}