package org.app.chatup.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.app.chatup.Client.FoursquareClient;
import org.app.chatup.R;

/**
 * Created by Vicky on 5/6/15.
 */
public class FoursquareLoginFragment extends BaseServiceLoginFragment<FoursquareClient> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_foursquare_login, container);

        return view;
    }

    @Override
    public void onCreate(Bundle saved) {
        super.onCreate(saved);
        setServiceName("Foursquare");
        setButtonResId(R.id.btn_foursquareLogin);
    }
}