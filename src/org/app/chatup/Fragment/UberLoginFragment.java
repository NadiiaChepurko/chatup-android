package org.app.chatup.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.app.chatup.Client.UberClient;
import org.app.chatup.R;

/**
 * Created by Vicky on 5/9/15.
 */
public class UberLoginFragment extends BaseServiceLoginFragment<UberClient> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_uber_login, container);

        return view;
    }

    @Override
    public void onCreate(Bundle saved) {
        super.onCreate(saved);
        setServiceName("Uber");
        setButtonResId(R.id.btn_uberLogin);
    }
}