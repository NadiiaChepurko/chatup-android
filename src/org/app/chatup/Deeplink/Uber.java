package org.app.chatup.Deeplink;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by nadii on 3/3/2016.
 */
public class Uber {

    private static final String UBER_APP_LINK = "uber://?action=setPickup";
    private static final String UBER_SITE_LINK = "https://m.uber.com/sign-up";
    private static final String TAG = "Uber";

    private static Location getLocation(Context context, String name, String address) {
        Location location = new Location(name, address);
        try {
            Geocoder geocoder = new Geocoder(context);
            List<Address> addresses = geocoder.getFromLocationName(address, 1);
            if(addresses.size() > 0) {
                location.latitude = addresses.get(0).getLatitude();
                location.longitude = addresses.get(0).getLongitude();
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return location;
    }

    public static Intent callUber(Context context, String destName, String destAddress){
        Location destination = getLocation(context, destName, destAddress);
        try {
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
            return getIntent(UBER_APP_LINK, destination);
        } catch (PackageManager.NameNotFoundException e) {
            // No Uber app! Open mobile website.
            return getIntent(UBER_SITE_LINK, destination);
        }
    }

    private static Intent getIntent(String baseUrl, Location destination) {
        try {
            Uri uri = buildUri(baseUrl, destination);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(uri);
            return intent;
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }

    private static Uri buildUri(String baseUrl, Location destination) throws UnsupportedEncodingException {
        return Uri.parse(baseUrl)
                .buildUpon()
                .appendQueryParameter("pickup", "my_location")
                .appendQueryParameter("dropoff[latitude]", String.valueOf(destination.latitude))
                .appendQueryParameter("dropoff[longitude]", String.valueOf(destination.longitude))
                .appendQueryParameter("dropoff[nickname]", destination.name)
                .appendQueryParameter("dropoff[formatted_address]", destination.address)
                .build();
    }

    private static class Location {
        public String name;
        public String address;
        public double latitude;
        public double longitude;

        private Location(String name, String address) {
            this.name = name;
            this.address = address;
        }
    }

}
