package org.app.chatup.Client;

import android.content.Context;
import com.codepath.oauth.OAuthBaseClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.scribe.builder.api.Api;
import org.scribe.builder.api.Foursquare2Api;

/**
 * Created by Vicky on 5/6/15.
 */
public class FoursquareClient extends OAuthBaseClient {
    public static final Class<? extends Api> REST_API_CLASS = Foursquare2Api.class;
    public static final String REST_URL = "https://api.foursquare.com/v2";
    public static final String REST_CONSUMER_KEY = "WEXJEPGUN0TH4TVJ55FCWC0N2Z2MEVZQX2L3YCN4XTGOAMZ0";
    public static final String REST_CONSUMER_SECRET = "RFA0QWQU1RP2UHFDMDS4BRKQFRNGGRG2XBMSOEDTGMC0CZV2";
    public static final String REST_CALLBACK_URL = "oauth://cbfoursquare.com";

    public FoursquareClient(Context context) {
        super(context, REST_API_CLASS, REST_URL, REST_CONSUMER_KEY, REST_CONSUMER_SECRET, REST_CALLBACK_URL, null);
    }

    // response.user.firstName
    public void getSelfUserInfo(AsyncHttpResponseHandler handler) {
        String apiUrl = getApiUrl("users/self");
        RequestParams params = new RequestParams();
        params.put("foo", "bar");
        params.put("bar", "baz");
        client.get(apiUrl, params, handler);
    }
}