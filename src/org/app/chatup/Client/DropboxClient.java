package org.app.chatup.Client;

import android.content.Context;
import com.codepath.oauth.OAuthBaseClient;
import org.scribe.builder.api.Api;
import org.scribe.builder.api.DropBox2Api;

/**
 * Created by Vicky on 5/9/15.
 */
public class DropboxClient extends OAuthBaseClient {
    public static final Class<? extends Api> REST_API_CLASS = DropBox2Api.class;
    public static final String REST_URL = "https://api.dropbox.com/1";
    public static final String REST_CONSUMER_KEY = "8e7oi27lp5vpico";
    public static final String REST_CONSUMER_SECRET = "c5kfi2mto9e5hiy";
    public static final String REST_CALLBACK_URL = "https://cbdropbox.com";

    public DropboxClient(Context context) {
        super(context, REST_API_CLASS, REST_URL, REST_CONSUMER_KEY, REST_CONSUMER_SECRET, REST_CALLBACK_URL, null);
    }
}
