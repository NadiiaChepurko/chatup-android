package org.app.chatup.Client;

import android.content.Context;
import com.codepath.oauth.OAuthBaseClient;
import org.scribe.builder.api.Api;
import org.scribe.builder.api.LiveApi;

/**
 * Created by Vicky on 5/9/15.
 */
public class MicrosoftClient extends OAuthBaseClient {
    public static final Class<? extends Api> REST_API_CLASS = LiveApi.class;
    public static final String REST_URL = "https://login.live.com/";
    public static final String REST_CONSUMER_KEY = "0000000044148C5E";
    public static final String REST_CONSUMER_SECRET = "8j-zoB2Vf5y5nQ7xue96Vs8mYoGNXjbp";
    public static final String REST_CALLBACK_URL = "http://www.cbmift.com";

    public MicrosoftClient(Context context) {
        super(context, REST_API_CLASS, REST_URL, REST_CONSUMER_KEY, REST_CONSUMER_SECRET, REST_CALLBACK_URL, "wl.skydrive");
    }

    // response.user.firstName
//    public void getSelfUserInfo(AsyncHttpResponseHandler handler) {
//        String apiUrl = getApiUrl("users/self");
//        RequestParams params = new RequestParams();
//        params.put("foo", "bar");
//        params.put("bar", "baz");
//        client.get(apiUrl, params, handler);
//    }
}