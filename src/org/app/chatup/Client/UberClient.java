package org.app.chatup.Client;

import android.content.Context;
import com.codepath.oauth.OAuthBaseClient;
import org.scribe.builder.api.Api;
import org.scribe.builder.api.Uber2Api;

/**
 * Created by Vicky on 5/9/15.
 */
public class UberClient extends OAuthBaseClient {
    public static final Class<? extends Api> REST_API_CLASS = Uber2Api.class;
    public static final String REST_URL = "https://api.uber.com/v1/";
    public static final String REST_CONSUMER_KEY = "6-EOQkLffUlOfdPXY7OjlXKrsCJTPXiz";
    public static final String REST_CONSUMER_SECRET = "BLLXVrCgLFs9LuamqC1J3FdNSui6POoiuqIFU1kI";
    public static final String REST_CALLBACK_URL = "oauth://cbuber.com";

    public UberClient(Context context) {
        super(context, REST_API_CLASS, REST_URL, REST_CONSUMER_KEY, REST_CONSUMER_SECRET, REST_CALLBACK_URL, "profile,request");
    }
}
