package org.app.chatup.AsyncTask;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import org.app.chatup.Activity.LoginActivity;
import org.app.chatup.R;
import org.app.chatup.Util.AsyncResponse;
import org.app.chatup.Util.Constants;
import org.app.chatup.Util.Util;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Random;

/**
 * Created by Vicky on 3/21/15.
 */
public class GCMRegisterTask extends AsyncTask<Void, Void, String> {
    private Context mContext;
    private GoogleCloudMessaging gcm;

    public AsyncResponse delegate = null;

    private String regId;
    private static final String TAG = "GCMRegisterTask";

    private final int MAX_ATTEMPTS = 5;
    private final int BACKOFF_MILLI_SECONDS = 2000;
    private final Random random = new Random();

    public GCMRegisterTask(Context context) {
        this.mContext = context;

        /**
         * Initializing it here also since ON_BOOT_COMPLETED redirects here
         * directly before starting LoginActivity.
         */
        if(Constants.appContext == null) {
            Constants.appContext = mContext.getApplicationContext();
        }
        if(Constants.keystorePass == null) {
            Constants.keystorePass = mContext.getResources().getString(R.string.keystorePass_ec2);
//            Constants.keystorePass = mContext.getResources().getString(R.string.keystorePass_mac);
        }

        gcm = GoogleCloudMessaging.getInstance(Constants.appContext);
    }

    @Override
    protected String doInBackground(Void... params) {
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        int noOfAttempts = 0;
        boolean stopTrying = false;
        String status = null;

        while(! stopTrying) {
            noOfAttempts++;
            Log.d(TAG, "Attempt #" + noOfAttempts + " to register");
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(Constants.appContext);
                }
                regId = gcm.register(Constants.SENDER_ID);
                status = "OK";

                // Registration successful. So stop trying
                stopTrying = true;
                sendRegistrationIdToBackend(regId, mContext);

                // Persist the regID - no need to register again.
                storeRegistrationId(regId);

            } catch (IOException ex) {
                /**
                 * If there is an error, don't just keep trying to register.
                 * Perform exponential back-off.
                 */
                status = "Error: " + ex.getMessage();
                Log.e(TAG, "Error: " + status);
                Util.saveToPrefs(null, Constants.PROPERTY_GCM_REG_ID, mContext);

                if(noOfAttempts == MAX_ATTEMPTS) {
                    Log.e(TAG, "Giving up registering for GCM");
                    stopTrying = true;
                }

                if(! stopTrying) {
                    try {
                        Thread.sleep(backoff);
                    } catch (InterruptedException ie) {
                        Log.e(TAG, "Thread interrupted. Aborting remaining retries");
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        if(status == null) {
            Log.wtf(TAG, "msg is NULL. This should never happen!");
        }

        return status;
    }

    @Override
    protected void onPostExecute(String status) {
        delegate.asyncTaskFinish(status);
    }

    /**
     * Sends the registration ID to the server over HTTP, so it can use GCM/HTTP to send
     * messages to this app.
     */
    private static void sendRegistrationIdToBackend(String regId, final Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("gcm_regId", regId);

        jsonObject.addProperty("email", Util.getProperty(Constants.PROPERTY_EMAIL, context, TAG));
        jsonObject.addProperty("name", "vlnvv");

        KeyManagerFactory kmf = null;
        KeyStore ks = null, ts = null;
        TrustManagerFactory tmf = null;
        SSLContext sslContext = null;
        Ion ion = Ion.getDefault(context);
        try {
            kmf = KeyManagerFactory.getInstance("X509");
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(context.getResources().openRawResource(R.raw.chatupcert_ec2), Constants.keystorePass.toCharArray());
//            ks.load(context.getResources().openRawResource(R.raw.chatupcert_win), Constants.keystorePass.toCharArray());
            kmf.init(ks, Constants.keystorePass.toCharArray());

            tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            ts = KeyStore.getInstance(KeyStore.getDefaultType());
            ts.load(context.getResources().openRawResource(R.raw.chatupcert_ec2), Constants.keystorePass.toCharArray());
//            ts.load(context.getResources().openRawResource(R.raw.chatupcert_win), Constants.keystorePass.toCharArray());
            tmf.init(ts);

            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            ion.configure().createSSLContext("TLS");
            ion.getHttpClient().getSSLSocketMiddleware().setTrustManagers(tmf.getTrustManagers());
            ion.getHttpClient().getSSLSocketMiddleware().setSSLContext(sslContext);
            ion.build(context)
                    .load(Constants.SERVER_REGISTER_URL)
                    .setJsonObjectBody(jsonObject)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject jsonObject) {
                            if (jsonObject == null || !jsonObject.has("response")  || !jsonObject.get("response").getAsString().equals("OK")) {
//                                Toast.makeText(context, "Server is busy. Please try again later.", Toast.LENGTH_SHORT).show();
                                Log.e(TAG, "GCM registration not successful. Something went wrong in backend");
                                return ;
                            }
                        }
                    });

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param regId registration ID
     */
    private void storeRegistrationId(String regId) {
        final SharedPreferences prefs = LoginActivity.getGcmPreferences(mContext);
        int appVersion = Util.getAppVersion(mContext);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.PROPERTY_GCM_REG_ID, regId);
        editor.putInt(Constants.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }
}
