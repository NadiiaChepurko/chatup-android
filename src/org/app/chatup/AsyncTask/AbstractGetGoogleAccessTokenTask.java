package org.app.chatup.AsyncTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import org.app.chatup.Util.Constants;

import java.io.IOException;

/**
 * Created by Vicky on 3/22/15.
 */
public abstract class AbstractGetGoogleAccessTokenTask extends AsyncTask<Void, Void, String> {
    private static final String TAG = "AbstractGetGoogleAccessTokenTask";

    protected Activity mActivity;
    protected String mScope;
    protected String mEmail;

    public AbstractGetGoogleAccessTokenTask(Activity activity, String email, String scope) {
        this.mScope = scope;
        this.mEmail = email;
        this.mActivity = activity;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            return fetchToken();
        } catch (IOException ex) {
            onError("Following Error occurred, please try again. " + ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    protected void onPostExecute(String token) {
        handleResult(token, mScope);
    }

    @SuppressLint("LongLogTag")
    protected void onError(String msg, Exception e) {
        if (e != null) {
            Log.e(TAG, "Exception: ", e);
        }
        show(msg);  // will be run in UI thread
    }

    /**
     * Get an authentication token if one is not available. If the error is not recoverable then
     * it displays the error message on parent activity.
     */
    protected String fetchToken() throws IOException {
        try {
            return GoogleAuthUtil.getToken(mActivity, mEmail, mScope);
        } catch (UserRecoverableAuthException userRecoverableException) {
            // Unable to authenticate, such as when the user has not yet granted
            // the app access to the account, but the user can fix this.
            // Forward the user to an activity in Google Play services.
            mActivity.startActivityForResult(userRecoverableException.getIntent(), Constants.REQ_RECOVER_PLAY_SERVICES_ERROR);
        } catch (GoogleAuthException fatalException) {
            onError("Unrecoverable error" + fatalException.getMessage(), fatalException);
        }
        return null;
    }

    /**
     * Show Toast message and then exit.
     * @param msg Error message to be displayed to the user.
     */
    protected void show(final String msg) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
            }
        });
        mActivity.finish();
    }

    protected abstract void handleResult(String token, final String scope);
}
