/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.app.chatup.GCM;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import org.apache.commons.lang3.StringUtils;
import org.app.chatup.Activity.Conversation;
import org.app.chatup.DB.Messages;
import org.app.chatup.DB.Websites;
import org.app.chatup.R;
import org.app.chatup.Util.Constants;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
    final String GROUP_KEY_MESSAGES = "group_key_messages";
    private static final String message_notify_bar = "You have unread messages";

    private NotificationManager mNotificationManager;
    private static String urlTitle;

    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static final String TAG = "GCM";


    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification(null, "Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification(null, "Deleted messages on server: " + extras.toString());
            // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                try {
                    JSONObject messageJson = new JSONObject(extras.getString(Constants.EXTRA_MESSAGE));

                    urlTitle = messageJson.getJSONObject("data").getString("urlTitle").trim();
                    String responseMsg = messageJson.getJSONObject("data").getString("message");
                    String requestMsg = null;
                    if (messageJson.getJSONObject("data").has("request")) {
                        requestMsg = messageJson.getJSONObject("data").getString("request");
                    }

                    // Post notification of received message.
                    sendNotification(requestMsg, responseMsg);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    private void sendNotification(String requestMsg, String responseMsg) {
        // if we got requestMsg from server and it was was redirected
        // to another vertical then we need to save redirected request as well
        if (requestMsg != null && !Constants.currentVertical.equals(urlTitle)) {
            new Messages(requestMsg, Websites.getWebsiteId(urlTitle), true).save();
        }
        if (StringUtils.isNoneBlank(responseMsg)) {
            new Messages(responseMsg, Websites.getWebsiteId(urlTitle), false).save();
        }

        Intent intent = new Intent(this, Conversation.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // in case we get response for another vertical (redirect request and response)
        // we should save
        Constants.currentVertical = urlTitle;

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack
        stackBuilder.addParentStack(Conversation.class);
        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(intent);
        PendingIntent contentIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.chatup_launcher);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.chatup_launcher)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(message_notify_bar)
                        .setStyle(new NotificationCompat.InboxStyle()
                                .addLine(responseMsg)
                                .setBigContentTitle(message_notify_bar)
                                .setSummaryText("vlnvv@gmail.com")
                        )
                        .setGroup(GROUP_KEY_MESSAGES)
                        .setGroupSummary(true)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        //.setDefaults(-1)    // -1 is DEFAULT_ALL
                        .setAutoCancel(true)
                        .setContentText(responseMsg)
                ;

        mBuilder.setContentIntent(contentIntent);

        if (! isRunningInForeground()) {
            // App is not running in foreground so create a notification
            mNotificationManager.notify(Constants.NOTIFICATION_ID, mBuilder.build());
        }

        /**
         * Create a new Intent for new incoming message and broadcast it.
         */
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("newMessageIntent"));
    }

    /**
     * @return true if app is running in foreground
     */
    public boolean isRunningInForeground() {
        return getApplicationContext().getPackageName().equalsIgnoreCase(((ActivityManager)getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1).get(0).topActivity.getPackageName());
    }
}
