package org.app.chatup.GCM;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import org.app.chatup.AsyncTask.GCMRegisterTask;
import org.app.chatup.Util.AsyncResponse;

/**
 * Created by Vicky on 3/21/15.
 */
public class UpdateReceiver extends BroadcastReceiver implements AsyncResponse {
    private GCMRegisterTask gcmRegisterTask;
    private static final String TAG = "UpdateReceiver";

    /**
     * Called when the app or OS is updated (or device rebooted).
     * GCM needs to be re-registered on every update.
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Intent " + intent.getAction() + " called. Re-registering GCM.");
        gcmRegisterTask = new GCMRegisterTask(context);
        gcmRegisterTask.delegate = this;
        gcmRegisterTask.execute();
    }

    @Override
    public void asyncTaskFinish(String asyncOutput) {
        return ;
    }
}
