package org.app.chatup.DB;

import android.database.Cursor;
import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Date;
import java.util.List;

/**
 * Created by Vicky on 1/9/15.
 */
@Table(name = "Messages")
public class Messages extends Model {

    @Column(name = "is_from_user", notNull = true, onNullConflict = Column.ConflictAction.ABORT)
    public boolean is_from_user;

    @Column(name = "message_body")
    public String message_body;

    // Foreign key
    @Column(name = "website_id", index = true, notNull = true, onNullConflict = Column.ConflictAction.ABORT, onDelete = Column.ForeignKeyAction.CASCADE)
    public Long website_id;

    @Column(name = "timestamp", notNull = true, onNullConflict = Column.ConflictAction.ABORT)
    public Date timestamp;

    @Column(name = "is_unread")
    public boolean is_unread;


    public Messages() {
        super();
    }

    public Messages(String message_body, Long website_id, boolean is_from_user) {
        super();

        this.message_body = message_body;
        this.website_id = website_id;
        this.is_from_user = is_from_user;

        if(this.is_from_user == true) {
            this.is_unread = false;
        } else {
            this.is_unread = true;
        }
        this.timestamp = new Date();
    }

    /**
     * Fetches all conversations between the user and provided website.
     * @param websiteTitle Title of the Website whose conversation is required.
     * @return A {@link Cursor} object, which is positioned before the first entry.
     */
    public static Cursor fetchConversationsFromTitle(String websiteTitle) {

        String resultRecords = new Select("*, Id as _id")
                .from(Messages.class)
                .where("website_id = ?")
                .toSql();

        Cursor resultCursor = Cache.openDatabase().rawQuery(resultRecords, new String[]{ String.valueOf(Websites.getWebsiteId(websiteTitle)) });

        return resultCursor;
    }

    public static Messages getLastConversation(String websiteTitle) {

        return new Select("*, Id as _id")
                .from(Messages.class)
                .where("website_id = ?", Websites.getWebsiteId(websiteTitle))
                .orderBy("timestamp DESC")
                .executeSingle()
                ;
    }

    public static List<Messages> getUnreadMessages(String websiteTitle) {
        return new Select("*, Id as _id")
                .from(Messages.class)
                .where("website_id = ? AND is_unread = ?", Websites.getWebsiteId(websiteTitle), true)
                .execute();
    }

    public static void markAllAsRead(String websiteTitle) {
        List<Messages> unreadMessages = getUnreadMessages(websiteTitle);

        for(Messages message : unreadMessages) {
            message.is_unread = false;
            message.save();
        }
    }

    public static int getUnreadCount(String websiteTitle) {
        return getUnreadMessages(websiteTitle).size();
    }
}