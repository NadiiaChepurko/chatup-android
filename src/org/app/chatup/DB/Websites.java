package org.app.chatup.DB;

import android.database.Cursor;
import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Date;
import java.util.List;

/**
 * Created by Vicky on 1/16/15.
 */

@Table(name = "Websites")
public class Websites extends Model {

    @Column(name = "website_title", unique = true, onUniqueConflict = Column.ConflictAction.ABORT, notNull = true, onNullConflict = Column.ConflictAction.ABORT)
    public String website_title;

    @Column(name = "favicon_url", notNull = true, onNullConflict = Column.ConflictAction.ABORT)
    public String favicon_url;

    @Column(name = "date_created", notNull = true, onNullConflict = Column.ConflictAction.ABORT)
    public Date date_created;

    @Column(name = "last_conversation_time")
    public Date last_conversation_time;

    @Column(name = "is_added", notNull = true, onNullConflict = Column.ConflictAction.ABORT)
    public int is_added;



    public Websites() {
        super();
    }

    public Websites(String website_title, String favicon_url) {
        super();

        this.website_title = website_title;
        this.favicon_url = favicon_url;

        this.date_created = new Date();
        this.last_conversation_time = null;
        this.is_added = 1;
    }



    // Return cursor for result set for all websites
    public static Cursor fetchAddedWebsites() {

        // Fetch all items with is_added equals 1
        String resultRecords = new Select("*, " + "Id as _id")
                .from(Websites.class)
                .where("is_added = ?")
                .orderBy("last_conversation_time DESC")
                .toSql();

        // Execute query on the underlying ActiveAndroid SQLite database
        Cursor resultCursor = Cache.openDatabase().rawQuery(resultRecords, new String[] {"1"});

        return resultCursor;
    }

    public static Cursor SearchWebsitesByTitle(String websiteTitle) {
        // Fetch all items with is_added equals 1
        String resultRecords = new Select("*, " + "Id as _id")
                .from(Websites.class)
                .where("website_title LIKE ?")
                .orderBy("last_conversation_time DESC")
                .toSql();

        // Execute query on the underlying ActiveAndroid SQLite database
        Cursor resultCursor = Cache.openDatabase().rawQuery(resultRecords, new String[] {"%"+websiteTitle+"%"});

        return resultCursor;
    }

    public static List<Websites> getAddedWebsites() {
        return new Select("*, Id as _id")
                .from(Websites.class)
                .where("is_added = ?", true)
                .orderBy("last_conversation_time DESC")
                .execute();
    }

    public static Cursor fetchWebsiteByName(String website_title) {

        String resultRecords = new Select("*, Id as _id")
                .from(Websites.class)
                .where("website_title = ?")
                .toSql();

        // Execute query on the underlying ActiveAndroid SQLite database
        Cursor resultCursor = Cache.openDatabase().rawQuery(resultRecords, new String[]{ website_title });

        return resultCursor;
    }

    public static Long getWebsiteId(String websiteTitle) {
        Websites website = new Select()
                .from(Websites.class)
                .where("website_title = ?", websiteTitle)
                .executeSingle();

        return website.getId();
    }
}
