package org.app.chatup;

import android.app.Application;
import com.activeandroid.ActiveAndroid;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

@ReportsCrashes(
        formUri = "https://vlnvv.cloudant.com/acra-chatup/_design/acra-storage/_update/report",
        reportType = HttpSender.Type.JSON,
        httpMethod = HttpSender.Method.POST,
        formUriBasicAuthLogin = "reetingententsisceetingi",
        formUriBasicAuthPassword = "0lIaMsKYCfGIBQjO0ipBNvis",
        formKey = "", // This is required for backward compatibility but not used
        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.STACK_TRACE,
                ReportField.LOGCAT,
                ReportField.USER_EMAIL
        },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.toast_crash
)
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // The following line triggers the initialization of ACRA
//        ACRA.init(this);

        // Initialize ActiveAndroid database
        ActiveAndroid.initialize(this);
    }
}
