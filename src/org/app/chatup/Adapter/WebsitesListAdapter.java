package org.app.chatup.Adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.koushikdutta.ion.Ion;
import org.app.chatup.DB.Messages;
import org.app.chatup.R;

/**
 * CursorAdapter for ListView
 * Displays Favicon and Website Title
 */
public class WebsitesListAdapter extends android.support.v4.widget.CursorAdapter {

    public WebsitesListAdapter(Context context, Cursor c, int flags) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.website_list_item, parent, false);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView websiteTitle = (TextView) view.findViewById(R.id.title);
        ImageView websiteFavicon = (ImageView) view.findViewById(R.id.favicon);
        TextView lastConversation = (TextView) view.findViewById(R.id.last_conversation);

        String title = cursor.getString(cursor.getColumnIndexOrThrow("website_title"));
        String faviconUrl = cursor.getString(cursor.getColumnIndexOrThrow("favicon_url"));
        String lastConversationMessage = "";


        if(Messages.getUnreadCount(title) > 0) {

            RectShape rect = new RectShape();
            ShapeDrawable rectShapeDrawable = new ShapeDrawable(rect);
            Paint paint = rectShapeDrawable.getPaint();
            paint.setColor(Color.parseColor("#FF2E8A06"));
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(12);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                view.setBackgroundDrawable(rectShapeDrawable);
            } else {
                setBg(view, rectShapeDrawable);
            }
            websiteTitle.setTextColor(Color.parseColor("#FF2E8A06"));
            lastConversation.setTextColor(Color.parseColor("#FF2E8A06"));
        }

        Messages messages = Messages.getLastConversation(title);
        if(messages != null) {
            lastConversationMessage = messages.message_body;
        }

        websiteTitle.setText(title);
        lastConversation.setText(lastConversationMessage);
        Ion.with(websiteFavicon)
                .placeholder(R.drawable.ic_stub)
                        //.error(R.drawable.ic_empty)
                        //.animateLoad(spinAnimation)
                        //.animateIn(fadeInAnimation)
                .load(faviconUrl);
    }

    /**
     * Does the same thing as setBackgroundDrawable().
     * A separate function just to avoid minSdkVersion error.
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void setBg(View view, ShapeDrawable rectShapeDrawable) {
        view.setBackground(rectShapeDrawable);
    }
}