package org.app.chatup.Adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.apache.commons.lang3.StringUtils;
import org.app.chatup.Deeplink.Uber;
import org.app.chatup.R;
import org.app.chatup.Util.Constants;
import org.app.chatup.Util.Util;

/**
 * Created by Vicky on 1/9/15.
 */
public class MessagesListAdapter extends android.support.v4.widget.CursorAdapter {

    private LayoutInflater cursorInflater;
    private String message_label;


    public MessagesListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);

        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        View view;

        int position = cursor.getPosition();
        int type = getItemViewType(position);

        if(type == 0) {
            view = cursorInflater.inflate(R.layout.conversation_bubble_right, parent, false);

        } else {
            view = cursorInflater.inflate(R.layout.conversation_bubble_left, parent, false);
        }

        holder.lblFrom = (TextView) view.findViewById(R.id.lblMsgFrom);
        holder.txtMsg = (TextView) view.findViewById(R.id.txtMsg);
        holder.lblMsgTime = (TextView) view.findViewById(R.id.lblMsgTime);
        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final ViewHolder holder = (ViewHolder) view.getTag();

        int position = cursor.getPosition();
        int type = getItemViewType(position);
        if(type == 0) {
            message_label = "You";
        } else {
            message_label = Constants.currentVertical;

            if (Constants.WEBSITES_YELP.equals(message_label)) {
                // if vertical is Yelp than make visible additional panel
                LinearLayout actionsLayout = (LinearLayout) view.findViewById(R.id.actions_layout);
                TextView openTable = (TextView) view.findViewById(R.id.lblOpenTable);
                TextView uber = (TextView) view.findViewById(R.id.lblUber);
                openTable.setVisibility(View.VISIBLE);
                uber.setVisibility(View.VISIBLE);
                actionsLayout.setVisibility(View.VISIBLE);

                uber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // if vertical is Yelp than make deeplink with uber
                        String[] tokens = holder.txtMsg.getText().toString().split("\n");
                        String name = tokens[0]; //get destination name
                        String address = tokens[2]; //get address from the message
                        Intent intent = Uber.callUber(Constants.appContext, name, address);
                        if (intent != null) {
                            context.startActivity(intent);
                        }
                    }
                });

                openTable.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // if vertical is Yelp than make deeplink with uber
                        String[] tokens = holder.txtMsg.getText().toString().split("\n");
                        String name = tokens[0]; //get destination name
                        String[] addressTokens = tokens[2].split(","); //get address from the message
                        String zip = addressTokens[addressTokens.length - 1]; //get zip from the address
                        String message = String.format(Constants.WEBSITES_OPEN_TABLE_MESSAGE_TEMPLATE, name, zip);
                        Util.goToConversationActivity(context, Constants.WEBSITES_OPEN_TABLE, message);
                    }
                });

            }
        }

        holder.lblFrom.setText(message_label);
        holder.txtMsg.setText(cursor.getString(cursor.getColumnIndexOrThrow("message_body")));

        long milli = Long.parseLong(cursor.getString(cursor.getColumnIndexOrThrow("timestamp")));
        holder.lblMsgTime.setText(DateUtils.getRelativeTimeSpanString(milli));
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        Cursor cursor = (Cursor) getItem(position);
        return getItemViewTypeHelper(cursor);
    }

    private int getItemViewTypeHelper(Cursor cursor) {
        int is_from_user = cursor.getInt(cursor.getColumnIndexOrThrow("is_from_user"));

        if(is_from_user == 1) {
            return 0;
        } else {
            return 1;
        }
    }

    public static class ViewHolder {
        public TextView lblFrom;
        public TextView txtMsg;
        public TextView lblMsgTime;
    }
}